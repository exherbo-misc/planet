#!/bin/bash

RUN_DATE=$(date -u +%Y-%m-%d_%H:%M)
LOG="${HOME}/planet-logs/planet-${RUN_DATE}.log"

{
    DIR=$(dirname $0)
    cd $DIR &&
    git clean -fdx &&
    git pull &&
    python2 planet.py configs/planet-exherbo.ini
} &> "$LOG"
